# Cyclobs user manual
> How to get/use data from https://cyclobs.ifremer.fr
### Notebook installation

conda create -n cyclobs python=3 cartopy

conda activate cyclobs

pip install xarray geoviews pandas rioxarray tqdm ipywidgets

wget https://gitlab.ifremer.fr/cyclobs/user_manual/-/raw/master/user_manual.ipynb

jupyter notebook --ip 0.0.0.0 --no-browser

jupyter notebook





```python
#import matplotlib
#%matplotlib inline 
import geoviews as gv
import geoviews.feature as gf
#gv.extension('bokeh') #  try this in a real notebook 
gv.extension('matplotlib')
import pandas as pd
import xarray as xr
import rasterio as rio
import rioxarray # geospatial extension for xarray
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy
from tqdm.auto import tqdm

download_path="/tmp/cyclobs"
os.makedirs(download_path,exist_ok = True)
```







<div class="logo-block">
<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAAB+wAAAfsBxc2miwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA6zSURB
VHic7ZtpeFRVmsf/5966taWqUlUJ2UioBBJiIBAwCZtog9IOgjqACsogKtqirT2ttt069nQ/zDzt
tI4+CrJIREFaFgWhBXpUNhHZQoKBkIUASchWla1S+3ar7r1nPkDaCAnZKoQP/D7mnPOe9/xy76n3
nFSAW9ziFoPFNED2LLK5wcyBDObkb8ZkxuaoSYlI6ZcOKq1eWFdedqNzGHQBk9RMEwFAASkk0Xw3
ETacDNi2vtvc7L0ROdw0AjoSotQVkKSvHQz/wRO1lScGModBFbDMaNRN1A4tUBCS3lk7BWhQkgpD
lG4852/+7DWr1R3uHAZVQDsbh6ZPN7CyxUrCzJMRouusj0ipRwD2uKm0Zn5d2dFwzX1TCGhnmdGo
G62Nna+isiUqhkzuKrkQaJlPEv5mFl2fvGg2t/VnzkEV8F5ioioOEWkLG86fvbpthynjdhXYZziQ
x1hC9J2NFyi8vCTt91Fh04KGip0AaG9zuCk2wQCVyoNU3Hjezee9bq92duzzTmxsRJoy+jEZZZYo
GTKJ6SJngdJqAfRzpze0+jHreUtPc7gpBLQnIYK6BYp/uGhw9YK688eu7v95ysgshcg9qSLMo3JC
4jqLKQFBgdKDPoQ+Pltb8dUyQLpeDjeVgI6EgLIQFT5tEl3rn2losHVsexbZ3EyT9wE1uGdkIPcy
BGxn8QUq1QrA5nqW5i2tLqvrrM9NK6AdkVIvL9E9bZL/oyfMVd/jqvc8LylzRBKDJSzIExwhQzuL
QYGQj4rHfFTc8mUdu3E7yoLtbTe9gI4EqVgVkug2i5+uXGo919ixbRog+3fTbQ8qJe4ZOYNfMoTI
OoshUNosgO60AisX15aeI2PSIp5KiFLI9ubb1vV3Qb2ltwLakUCDAkWX7/nHKRmmGIl9VgYsUhJm
2NXjKYADtM1ygne9QQDIXlk49FBstMKx66D1v4+XuQr7vqTe0VcBHQlRWiOCbmmSYe2SqtL6q5rJ
zsTb7lKx3FKOYC4DoqyS/B5bvLPxvD9Qtf6saxYLQGJErmDOdOMr/zo96km1nElr8bmPOBwI9COv
HnFPRIwmkSOv9kcAS4heRsidOkpeWBgZM+UBrTFAXNYL5Vf2ii9c1trNzpYdaoVil3WIc+wdk+gQ
noie3ecCcxt9ITcLAPWt/laGEO/9U6PmzZkenTtsSMQ8uYywJVW+grCstAvCIaAdArAsIWkRDDs/
KzLm2YcjY1Lv0UdW73HabE9n6V66cxSzfEmuJssTpKGVp+0vHq73FwL46eOjpMpbRAnNmJFrGJNu
Ukf9Yrz+3rghiumCKNXXWPhLYcjxGsIpoCMsIRoFITkW8AuyM8jC1+/QLx4bozCEJIq38+1rtpR6
V/yzb8eBlRb3fo5l783N0CWolAzJHaVNzkrTzlEp2bQ2q3TC5gn6wpnoQAmwSiGh2GitnTmVMc5O
UyfKWUKCIsU7+fZDKwqdT6DDpvkzAX4/+AMFjk0tDp5GRXLpQ2MUmhgDp5gxQT8+Y7hyPsMi8uxF
71H0oebujHALECjFKaW9Lm68n18wXp2kVzIcABytD5iXFzg+WVXkegpAsOOYziqo0OkK76GyquC3
ltZAzMhhqlSNmmWTE5T6e3IN05ITFLM4GdN0vtZ3ob8Jh1NAKXFbm5PtLU/eqTSlGjkNAJjdgn/N
aedXa0tdi7+t9G0FIF49rtMSEgAs1kDLkTPO7ebm4IUWeyh1bKomXqlgMG6kJmHcSM0clYLJ8XtR
1GTnbV3F6I5wCGikAb402npp1h1s7LQUZZSMIfALFOuL3UUrfnS8+rez7v9qcold5tilgHbO1fjK
9ubb17u9oshxzMiUBKXWqJNxd+fqb0tLVs4lILFnK71H0Ind7uiPgACVcFJlrb0tV6DzxqqTIhUM
CwDf1/rrVhTa33/3pGPxJYdQ2l2cbgVcQSosdx8uqnDtbGjh9SlDVSMNWhlnilfqZk42Th2ZpLpf
xrHec5e815zrr0dfBZSwzkZfqsv+1FS1KUknUwPARVvItfKUY+cn57yP7qv07UE3p8B2uhUwLk09
e0SCOrK+hbdYHYLjRIl71wWzv9jpEoeOHhGRrJAzyEyNiJuUqX0g2sBN5kGK6y2Blp5M3lsB9Qh4
y2Ja6x6+i0ucmKgwMATwhSjdUu49tKrQ/pvN5d53ml2CGwCmJipmKjgmyuaXzNeL2a0AkQ01Th5j
2DktO3Jyk8f9vcOBQHV94OK+fPumJmvQHxJoWkaKWq9Vs+yUsbq0zGT1I4RgeH2b5wef7+c7bl8F
eKgoHVVZa8ZPEORzR6sT1BzDUAD/d9F78e2Tzv99v8D+fLVTqAKAsbGamKey1Mt9Ann4eH3gTXTz
idWtAJ8PQWOk7NzSeQn/OTHDuEikVF1R4z8BQCy+6D1aWRfY0tTGG2OM8rRoPaeIj5ZHzJxszElN
VM8K8JS5WOfv8mzRnQAKoEhmt8gyPM4lU9SmBK1MCQBnW4KONT86v1hZ1PbwSXPw4JWussVjtH9Y
NCoiL9UoH/6PSu8jFrfY2t36erQHXLIEakMi1SydmzB31h3GGXFDFNPaK8Rme9B79Ixrd0WN+1ij
NRQ/doRmuFLBkHSTOm5GruG+pFjFdAmorG4IXH1Qua6ASniclfFtDYt+oUjKipPrCQB7QBQ2lrgP
fFzm+9XWUtcqJ3/5vDLDpJ79XHZk3u8nGZ42qlj1+ydtbxysCezrydp6ugmipNJ7WBPB5tydY0jP
HaVNzs3QzeE4ZpTbI+ZbnSFPbVOw9vsfnVvqWnirPyCNGD08IlqtYkh2hjZ5dErEQzoNm+6ykyOt
Lt5/PQEuSRRKo22VkydK+vvS1XEKlhCJAnsqvcVvH7f/ZU2R67eXbMEGAMiIV5oWZWiWvz5Fv2xG
sjqNJQRvn3Rs2lji/lNP19VjAQDgD7FHhujZB9OGqYxRkZxixgRDVlqS6uEOFaJUVu0rPFzctrnF
JqijImVp8dEKVWyUXDk92zAuMZ6bFwpBU1HrOw6AdhQgUooChb0+ItMbWJitSo5Ws3IAOGEOtL53
0vHZih9sC4vtofZ7Qu6523V/fmGcds1TY3V36pUsBwAbSlxnVh2xLfAD/IAIMDf7XYIkNmXfpp2l
18rkAJAy9HKFaIr/qULkeQQKy9zf1JgDB2uaeFNGijo5QsUyacNUUTOnGO42xSnv4oOwpDi1zYkc
efUc3I5Gk6PhyTuVKaOGyLUAYPGIoY9Pu/atL/L92+4q9wbflRJ2Trpm/jPjdBtfnqB/dIThcl8A
KG7hbRuKnb8qsQsVvVlTrwQAQMUlf3kwJI24Z4JhPMtcfng5GcH49GsrxJpGvvHIaeem2ma+KSjQ
lIwUdYyCY8j4dE1KzijNnIP2llF2wcXNnsoapw9XxsgYAl6k+KzUXbi2yP3KR2ecf6z3BFsBICdW
nvnIaG3eHybqX7vbpEqUMT+9OL4Qpe8VON7dXuFd39v19FoAABRVePbGGuXTszO0P7tu6lghUonE
llRdrhArLvmKdh9u29jcFiRRkfLUxBiFNiqSU9icoZQHo5mYBI1MBgBH6wMNb+U7Pnw337H4gi1Y
ciWs+uks3Z9fztUvfzxTm9Ne8XXkvQLHNytOOZeiD4e0PgkAIAYCYknKUNUDSXEKzdWNpnil7r4p
xqkjTarZMtk/K8TQ6Qve78qqvXurGwIJqcOUKfUWHsm8KGvxSP68YudXq4pcj39X49uOK2X142O0
Tz5/u/7TVybqH0rSya6ZBwD21/gubbrgWdDgEOx9WUhfBaC2ibcEBYm7a7x+ukrBMNcEZggyR0TE
T8zUPjikQ4VosQZbTpS4vqizBKvqmvjsqnpfzaZyx9JPiz1/bfGKdgD45XB1zoIMzYbfTdS/NClB
Gct0USiY3YL/g0LHy/uq/Ef6uo5+n0R/vyhp17Klpge763f8rMu6YU/zrn2nml+2WtH+Z+5IAAFc
2bUTdTDOSNa9+cQY7YLsOIXhevEkCvzph7a8laecz/Un/z4/Ae04XeL3UQb57IwU9ZDr9UuKVajv
nxp1+1UVIo/LjztZkKH59fO3G/JemqCfmaCRqbqbd90ZZ8FfjtkfAyD0J/9+C2h1hDwsSxvGjNDc
b4zk5NfrSwiQblLHzZhg+Jf4aPlUwpDqkQqa9nimbt1/TDH8OitGMaQnj+RJS6B1fbF7SY1TqO5v
/v0WAADl1f7zokgS7s7VT2DZ7pegUjBM7mjtiDZbcN4j0YrHH0rXpCtY0qPX0cVL0rv5jv/ZXend
0u/EESYBAFBU4T4Qa5TflZOhTe7pmKpaP8kCVUVw1+yhXfJWvn1P3hnXi33JsTN6PnP3hHZ8Z3/h
aLHzmkNPuPj7Bc/F/Q38CwjTpSwQXgE4Vmwry9tpfq/ZFgqFMy4AVDtCvi8rvMvOmv0N4YwbVgEA
sPM72/KVnzfspmH7HQGCRLG2yL1+z8XwvPcdCbsAANh+xPzstgMtxeGKt+6MK3/tacfvwhWvIwMi
oKEBtm0H7W+UVfkc/Y1V0BhoPlDr/w1w/eu1vjIgAgDg22OtX6/eYfnEz/focrZTHAFR+PSs56/7
q32nwpjazxgwAQCwcU/T62t3WL7r6/jVRa6/byp1rei+Z98ZUAEAhEPHPc8fKnTU9nbgtnOe8h0l
9hcGIqmODLQAHCy2Xti6v/XNRivf43f4fFvIteu854+VHnR7q9tfBlwAAGz+pnndB9vM26UebAe8
SLHujPOTPVW+rwY+sxskAAC2HrA8t2Vvc7ffP1r9o+vwR2dcr92InIAbKKC1FZ5tB1tf+/G8p8sv
N/9Q5zd/XR34LYCwV5JdccMEAMDBk45DH243r/X4xGvqxFa/GNpS7n6rwOwNWwHVE26oAADYurf1
zx/utOzt+DMKYM0p17YtZZ5VNzqfsB2HewG1WXE8PoZ7gOclbTIvynZf9JV+fqZtfgs/8F/Nu5rB
EIBmJ+8QRMmpU7EzGRsf2FzuePqYRbzh/zE26EwdrT10f6r6o8HOYzCJB9Dpff8tbnGLG8L/A/WE
roTBs2RqAAAAAElFTkSuQmCC'
     style='height:25px; border-radius:12px; display: inline-block; float: left; vertical-align: middle'></img>




  <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAAFMAAABTABZarKtgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAArNSURB
VFiFnVd5VFNXGv/ee0kgGyQhbFoXIKCFYEXEDVErTucMoKUOWA/VLsNSLPQgFTOdyrHPiIp1lFIQ
OlaPShEG3EpPcQmISCuV1bQ1CLKIULeQhJA9JO+9+UMT0x5aPfOdc895373f/e7v/t537/ddBF5Q
JBIJl81mJwCACEVRQBCEQhAEAQCgnghCURRCkmS7Wq2+WlJSYn0Rv8jzDHAcD0EQJIVGo5mFQuGF
jIyMu39kq1KpkOrq6gU6nS6aIAiGzWY7VVBQ0P9/AcjNzWXy+fxcOp2uiY+Przm0d6+n8dblv/Fo
kzM4SzYfPlRePvFnjnt6ehh1dXVv2mw2nlar/byoqMj8wgBwHBchCJIZEhJSeu1yHVi7vtu02t8+
NykQ7BMWoOUMhXQsXLv5IQAwSJJEEASxcDicoeTk5DtCoZBy9XX69Gnv3t7ebJIky3EcH3guAKlU
GoGiaOKWLVsOvhs7/9XXPMde3/IyIFbMnaPDuD5AUdQuOf2XlD0npTExMWYAgNbWVpZcLg8xGAzB
JEnSvby82tPT052LaTQatLy8fBtJkt/s3Lnz5h8CwHFcRKPRNu/YsePAjh072KTs0IGCxRg8RgUB
TGpSx6cmHgMAfNqN6Xa1GvJ/D35gYAAViURkcXHxUrPZHDRv3rxv4uLiDI7xPXv2bLdYLBUFBQWD
jj7M8ZGbm8tkMpmSrKysQiaTScXGxtpqL7dManT6tcu5mgEWWJyOhicozpk+c3NsbKzNFcBbWWEf
1Td9/upA30i3ZJv0h8bGxiSFQmFcuHDhOACAWCy+0d3dvX3lypUtzc3N9t8AiIuLk4SEhByLiooy
AgAcO3ZsNlPgH3Cttb35JZo+bCYXIQAA9MDiUW7sWS1KN687w6Mera2twa2trfMvXboUOS28Pyb1
U08McRtf/sXBSmt5cc35pqamVQqFwhoZGallMpnU/fv3e7RaberVq1d/AABAn1IfQqfTNRs3blQB
AFy+fJk7Nja2XCKRnD3dNSorusPq6NfTPR+gPiEEoLRFXO1tS2+zavv27ReftjNttyr0S1/j0rUP
PEJQwNwQYGgAACQSyXmNRhMtk8lYAAApKSlKDMP0+fn5QU4ACIKkxMfH1zjYuHnz5uspKSlOfdX7
u68fvOePcCzKQR4YVCgATGfa/F3pnzaHWOAXSDyaMCqH2+r8VXErP3D+snXr1tV2dXW94dATExOr
6XT6JgAAVCKRcDEMM4WHh9sAAHJyUqNu//wDymKx7AAAVVVVPiaTKXxByrYMvBsxEMSTwPXhuL+8
e/fu9fv371+flvbemogYNz+TnsBOFEwMFO8/KzEYDKFVVVX+AAChoaGT7u7ud48ePRro0DEMs+bl
5bFRNpud4O3tfdGBzq5uy/5wTUPM/q2zC9atmbVqeHg4Pi0t7WxGRoZFH5rw76I7LI8HqHfwPL7d
rfVagzw1NfW81t4ePUfsP/OrnWZ6fPSuUqFQSEkkkrOjo6OvuQR5q0ajiXLoPj4+lzgcTjwKACLH
9SqXy2kzhBO8haGo+UA2wZW+p880DxeveGt9aHx9fT09ctlq3sC0NT9e6xsbjuZblSxl7wKtVotM
m6PnXvlmZJBtX91CEMQsxyJsNlteXl4udugIghAajQYFAEhPTx9AEGQOimGY8y4oLt63KlJkdB4t
P282Z/c/dPrDH04ktJ9P2tfWXP3+2o1vHzunEp6Xq0lsGt08KzUrcSGTQ3n3XeefLCs5UqnT6Rap
VCoEACA7O/snvV4f5gJooLa2NsihoygKKEVRzquTND2OCpttGXdG1tOxwOlgzdvE9v30rV+m3W5I
2jfJNQmLH85QUUzPNTwvkAx0+vVGhq2/VV9fT+dyuZ01NTXOXQOA3fGxevXq2waDYY5r8KIoij5b
jzB5Cz2oKdOo0erOm+1tVuVtBMZXElNMRJR1fvvjx9iPLQ/RjpuB0Xu/Vp7YmH1864YNG3oNBkPw
VD7mzp1rJUnSzZUBmqsBggAgGFC/n6jVA+3WoN3tu1Gg39cg2tEx1Cg3CIJHsclxnl2HRorMN8Z0
fRW+vr7GJ36Q56Z5h9BIknzGAMJWtvdQYs0EZe3/FSwqk5tpXEMb1JoYD+n8xRdQJl/fMPEgzKhS
L40KCD7lGzg92qIyovpb3y/msT2un2psvFpWVvYyl8vtc1nDSXFXV5c7iqLOtEyS5LNBAADfWeKm
Ly4uuvR1++sfv51/P5sfnHm2/Iy+mBmwsaHJbpt+Q0jHSS7TZ/PSNVkNJ/973OxtemD1s91CPb12
h9MfvZsk5meo1eqo5ORkxTNWn7HR1tY2l8PhOAsUiqIolCRJcETtv/61qzNySYK5trZ2TCgUUiwW
S1FSUhLR+bA/kAzwXcAbHa/cFhrTXrJ/v+7IkSPu3Je4Xm5eboJv2wba5QbO5fQwxhsP679Y+nFO
jgAAoKSkJILFYjnBGI1G0YYNGwYBnqRoiqIQlKKojurq6gUAAAKBgKQoiuGYkJWVpTCZTOKmI1Xd
HwnDcm+cOnOMw+H0FxYWbqpvqv/r9EV+bky+O+/QoUPiqJRt9JphTLFHbKBCR87tWL9EPN9oNIZn
ZWUpXHaMCQQCEgCgsrIyEgBuoGq1+qpOp4t2GPH5/BvFxcVLHXpgYGDD8ePH/56Xl2cCAMjMzOxP
S0s7pWfow4RCbz/fAF9RT0+P9yeffHJySSqev+9nxLD1FaAlTR8vlJ8vxxzsFhUVLRMIBB0OvwaD
YRlFUdfQkpISK0EQ9J6eHgYAQEZGxl2z2Rw0MjJCBwBITk5+xOVyfzpw4ECSw5lQKKQIbxtJm4EN
8eZ7jPz0oNv+dK5FG/jq54eH+IFr/S1KabBy0UerAvI+++wzD4vFEpCWljYEACCTyVh2ux3FcXwS
BQCw2WxVdXV1bzrQRURE1FVVVTn1zMzM/pkzZ35/9OjRd0pLS19RqVQIy4/tCwDgOcPTQvFQEQBA
aWnpK0ERK2LbyVllN341GUJ4YDu8zD5bKyur7O+85tx9Z2fnO1ar9QjA04KkpaVFs2LFir8olcq7
YWFhJpFINNnX16drbGyMjY6Ovg0AIBaLjcuXL5d3d3d7XbhwIW704b3F479MeD1qVfJ5Og/bvb4R
LwaDMZabm9uwflNa/z/3HOIv5NsDEK7XS7FeevXPvYNLvm5S/GglCK5KpZorlUobXE8g5ObmMqVS
6UG1Wu1BURSHoijOiRMnwgoLC7coFAqBo+9Fm0KhEKStmvvto3TeucFN7pVJYbytarXaQyqVHsRx
3N15TF1BuBaljr4rV66wOzo63mAymXdzcnKuwwtIUVHRMqvVGkgQxMV7NXvyJijGvcNXB/7z5Zdf
bicI4gSO40NTAgD4bVnuODIAT2pElUq1FEEQO4fD6QsPD++fqixHEATj8/ntjoCrqKhwS0hIsJWV
leURBHEOx3G563pT3tn5+flBDAbjg6CgoMMpKSlK17GhoSFMJpMFPk04DJIkEQzDzCwW6+5UD5Oa
mhrfO3fufECS5GHXnf8pAAAAHMfdURTdimGYPjExsTo0NHTyj2ynEplMxurs7HyHIAiKJMlSHMct
U9k9N2vl5+cH0en0TRiGWX18fC65vnh+LxqNBq2oqFhgMpmi7XY7arVaj+zdu/fxn/l/4bSZl5fH
5nK5CQAQMtXznCRJePpEbwOAZhzHX4ix/wHzzC/tu64gcwAAAABJRU5ErkJggg=='
       style='height:15px; border-radius:12px; display: inline-block; float: left'></img>



</div>




```python
# FIXME : year=2018 will help to select HECTOR in 2018
# FIXME : date format is MM-DD-YYYY
# FIXME : adding 'VMAX' to includeCols give 'INTERNAL SERVER ERROR'
# FIXME : check if all arguments are valids
# FIXME : add includeCols=all

request_url="https://cyclobs.ifremer.fr/app/api/getData?Cyclone%20name=HECTOR&startdate=01-01-2018&stopdate=12-31-2018&instrument=C-Band%20SAR&includeCols=Cyclone%20name,File%20path,Instrument,Mission,SID,Basins"
df_request = pd.read_csv(request_url)

# FIXME : avoid %20 in url

# FIXME : Cyclone%20name not handled (all cyclones returned)
df_request = df_request[df_request["Cyclone name"] == "HECTOR"]

# FIXME :  "Instrument" not handled (all instruments returned) 
df_request = df_request[df_request["Instrument"] == "C-Band Synthetic Aperture Radar"].reset_index()

# FIXME  : rename 'File path' to 'url' or 'File url' in upstream
df_request.rename(columns={"File path": "url"},inplace=True)


# FIXME : _gd instead of _sw by default
df_request['url'] = df_request['url'].map(lambda x : x.replace("_sw","_gd"))

# add download path
df_request['path'] = df_request['url'].map(lambda x : os.path.join(download_path,os.path.basename(x)))
df_request
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>Cyclone name</th>
      <th>url</th>
      <th>Instrument</th>
      <th>Mission</th>
      <th>SID</th>
      <th>Basins</th>
      <th>path</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>718</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180803t142144-20180...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>719</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180805t150354-20180...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>720</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180807t154512-20180...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>734</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180803t022503-201...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>735</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180803t143803-201...</td>
    </tr>
    <tr>
      <th>5</th>
      <td>737</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180807t154302-201...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>738</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180808t041303-201...</td>
    </tr>
    <tr>
      <th>7</th>
      <td>740</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180812t172848-201...</td>
    </tr>
    <tr>
      <th>8</th>
      <td>809</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180804t142907-201...</td>
    </tr>
    <tr>
      <th>9</th>
      <td>810</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180806t033804-201...</td>
    </tr>
    <tr>
      <th>10</th>
      <td>812</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180810t044504-201...</td>
    </tr>
    <tr>
      <th>11</th>
      <td>813</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180810t165606-201...</td>
    </tr>
    <tr>
      <th>12</th>
      <td>814</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180811t052613-201...</td>
    </tr>
  </tbody>
</table>
</div>




```python
# download 'url' to 'path' with wget, and read files
projection=ccrs.PlateCarree()
datasets = []
for idx,entry in tqdm(df_request.iterrows(),total=df_request.shape[0]):
    ret = os.system('cd %s ; wget -N  %s' % (os.path.dirname(entry['path']),entry['url']))
    # FIXME : rename 'X' and 'Y' to 'x' and 'y'
    if ret == 0 : 
        ds = xr.open_dataset(entry['path']).rename({'X' : 'x', 'Y':'y'})
       
        datasets.append( ds.rio.reproject(projection.proj4_params))
        
    else:
        datasets.append(None) # error fetching file
df_request['dataset'] = datasets
df_request
#ds = xr.open_dataset(df_request.iloc[2]['File path'])
```


    HBox(children=(FloatProgress(value=0.0, max=13.0), HTML(value='')))


    





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>Cyclone name</th>
      <th>url</th>
      <th>Instrument</th>
      <th>Mission</th>
      <th>SID</th>
      <th>Basins</th>
      <th>path</th>
      <th>dataset</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>718</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180803t142144-20180...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>719</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180805t150354-20180...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>720</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>RADARSAT-2</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/rs2--owi-cm-20180807t154512-20180...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>734</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180803t022503-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>735</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180803t143803-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>5</th>
      <td>737</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180807t154302-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>738</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180808t041303-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>7</th>
      <td>740</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 A</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1a-ew-owi-cm-20180812t172848-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>8</th>
      <td>809</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180804t142907-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>9</th>
      <td>810</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180806t033804-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>10</th>
      <td>812</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180810t044504-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>11</th>
      <td>813</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NEP;NWP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180810t165606-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
    <tr>
      <th>12</th>
      <td>814</td>
      <td>HECTOR</td>
      <td>https://cyclobs.ifremer.fr/static/sarwing_data...</td>
      <td>C-Band Synthetic Aperture Radar</td>
      <td>SENTINEL-1 B</td>
      <td>ep102018</td>
      <td>NWP;NEP</td>
      <td>/tmp/cyclobs/s1b-ew-owi-cm-20180811t052613-201...</td>
      <td>[lon, lat, wind_speed, wind_to_direction, mask...</td>
    </tr>
  </tbody>
</table>
</div>




```python

gv_list=[gf.coastline.opts(projection=ccrs.Mercator())]
for ds in df_request['dataset']:
    gv_list.append(gv.Image((ds['x'],ds['y'],ds['wind_speed'].squeeze()),crs=ccrs.Mercator()))
    
plot=gv.Overlay(gv_list)

```


```python
plot
```




<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAABbCAYAAAB3e+DlAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjMsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+AADFEAAAgAElEQVR4nO2deXRU5f3/X89kspAQsCCEJcoiq4EkUISAylJkUREVVKBYU0V/Hu1Pj2Kh+AWOWKHUBQWXHz1qOVJ/tlatrYggolRKDfxE1Cqo0ApUdkRiIGSb5fn98dzP3GfCZGFCFuS+z5kzM3fu8tw79/N+PvtVWmuNBw8eznr4GnsAHjx4aBrwyMCDBw+ARwYePHhw4JGBBw8eAI8MPHjw4MAjAw8ePADgb+wBVIXOnTvz3//+t7GH4cFDk0WnTp3YvXv3adufaqp5BkopmujQPHhoEjjdMuKZCR48eABqQQZ79uxhxIgR9O7dm6ysLJYsWRL1+2OPPYZSiiNHjgAQDoe56aabGDJkCNu2bQPg/fffRynFm2++Gdlu3LhxvP/++6fxVOAapbhCKSY6r3yf4lqlmKIU1zgvDx48xEaNZOD3+1m0aBFffvklmzZt4plnnuGLL74ADFGsXbuW888/P7L+O++8w6BBg/jrX//KokWLIsszMzNZsGBBPZyCNVYgHUgA0hQkAklAKdAMUMB4ZQjiMqUY7xDEtR5JePBQMxm0b9+e/v37A5Cenk7v3r3Zt28fAPfeey+PPPIIyhKmUCiEz+fD5/NF2TM5OTm0bNmStWvXnu5zcI8NJDhDScKcXKKCHynzWwhDFBroqqCdgrbO7x48nO04JZ/B7t27+eSTTxg0aBArVqygY8eO5OTkRK0zZswY1q9fz/jx45k+fXrUb3PmzGH+/Pl1H3UVuNAHzTFCnmgtP64hDKRhSEIBxUCJ8wrV24g8eDhzUOvQYnFxMRMnTmTx4sX4/X4WLFjAO++8c/IO/X5efvnlmPu49NJLAdiwYUOcw60eSUBrZUyFQxq+xZBCIhBwXiFnvXTru0cGHjzUUjMIBAJMnDiRqVOnMmHCBL7++mt27dpFTk4OnTt3Zu/evfTv35+DBw/WuK/Zs2fXi+/gtSTFcW0E/5B2TywdSFLGNEjACH66gu+tiEzCaR+NBw9nHmokA60106ZNo3fv3hG1v2/fvhw+fJjdu3eze/duMjMz+fjjj2nXrl2NBxw9ejSFhYX861//qvvoLbwUgOYKyrQRdgVkYMwDkXsfUIHRCFIVJAMpwDENY5TiSsep6MHD2YgayeCDDz7gxRdfZN26deTm5pKbm8uqVavqdNDZs2ezd+/eOu2jMi5JgH4JJopQog0B2ETgx2gFKcBRDYXaaAd7nXVTgSAm6nCDRwgezkLUSAadOnVi+PDhBAIBAoEAN998M1dccQUzZsygV69eZGdn069fP/x+1/0wY8YMBgwYwPr16wGTWvzWW2/x1FNPATB+/Hh+8YtfnNZUygsTzIzvB8own0uBXRoOaPgOKMJoBmCIogxDDmEMESQ4v3s+BA9nI+LOMxg1ahRbt27ls88+o0ePHixcuBCAr776CoB//OMfPPPMM5H9tG3bliVLllBRURHzOHXF7yugMAxHtCGEvRr2addXIK8wRtjlxMPOZyGApkAEx48f59NPP23sYXg4yxB3nsHo0aMj2kBeXl5E7Zc8g8p5023atGHkyJEsX778tJ/ET32KdAUrQnBQQznGZyBH1xj/ABgHo8KNIvic78m4oZXGrN4KBAK0atWKfv368fbbbzfiSDycbYg7z8DGsmXLuPzyywHIysqipKSESy65hDvuuCNqvVmzZrFo0SJCodM7/8rM7geOYvwB4Ap1Au6JKowpkIQhiVgjqWl0VyjFGOf1E8fxOMZJha4r/H4/s2bNwu/3k5qaGlleXFzMrl276qV4S5ynY513D2cn4sozaNGiRWT5ggUL8Pv9TJ06NbJMfAOV0aVLFwYOHMgf//jHOgz5ZBRpaKWMj0ASiipwTQMw5oA4FMV0UBhSkN8TMX4EgOuU4rUqBC/svIcwSU5yLIURLCGcCtzkpwTg3VoIslKKhx56iIceeiiyTGtN69atqaio4Mknn+Suu+6qcT/VQdKvJSMTa5wpddqzhzMZtSKDynkGguXLl7Ny5Uree++9qJTk6vA///M/XHfddQwdOjS+EcdAMZCqzY1dghFwSS5KwghvBdACQxipzncxEcBsm6ygQrvbVoVE4ASuIInWYfsd0oAfOd8V8P0pnM8kpShxPvsxAnrnPfewePFi7r77brp27cqrr77K2LFjmTx5crX7Em1FWeOVlGw7FyOEiaR4ReNnL+LKMwB4++23efjhh1mxYkWUOlsTevXqxYUXXsjKlSvjG3EMJGNIQFyTAdxogcz4UrAUcNZRziuIW89Q7hBKorWsMsYqRSlmvRTMBayctCRmizgrQ0CrasZ/g1Nl+VOfYpIjvM2cMctMvWjRInbs2MHWrVtZu3Yty5cvZ8qUKVx66aUsWbKE8vLymGMVhJxz9VljlplAiEHGXBV+ohQXOyaRhx8eaiSD119/nRdffJHf/e53pKSk0LFjR1atWsWdd97Jzp07yczMJD09nZtvvhmIXcK8adMmtm3bFilhnj17Nnv27IlEHuqCS5xZNICZfUULEHMAzI0vM30irjDIyfsxwi9+ByGMWNDOvhJx1eyw8x501pFZ2Cf7xtVAJirFdY7wx6qYTHCyJZUzjmOYkKjP56N79+5kZWWxePFitNY8/vjj/POf/+See+7h0UcfjTleMY8qj1Wui5gHfut6XVeFsAcxWhWY6z5MKUY4fhN5H6gUeR5ZnJGokQyGDBnCli1bKCsr49tvvyUtLY3OnTtz3XXXMW/ePMrLy5k9ezZt27YFYpcwt2vXLqqEOScnhyuvvJKxY8eelpOQ2SwJd8YWzeA7jCCIgCnccKJ89le6ED6gKvNeBElMCdE8whjhFTHwO68UZdKhE5QRMolyCEHY1ZTlGoLakFkJDgngajyVcc899zBv3jxuu+02hg8fTjAYjLmeD1fo5RopXFKUMcmyWKf+E6VIx2hXMh77/IUAmzufByvFUEeT8HBmIO7Q4htvvEF+fj4A+fn5/O1vfwMavoT5GEaYSp3vxRgh0piZTJKPKnATiwTpyhCH+A9EEJIwpc9TYtzIMrNKKLLYWa5xqyLlrCuAkCPgQU2UQ1L2k+CMscxZvxTjjyjE1T6qEielFA888ADPPvssQ4cO5bzzzmPp0qWRXA47giLkJJmZSQqU85KaDR/Gb1IVSnDNiTRcP0OAaBJpifGXNAdaV707D00McYcWDx06RPv27QFDGIcPHwYavoQ5ETiMSwoSDQhh8g3kBGUWqzzriZ0cBpopU98g/oJmMQQjkWj1WmZameGDGIFOwa2WBHjFIQL7gss2GkMEJ5yX2PYSFakp1Pnll1/SvHlznn76aV599VWuuuoqQqFQZKbHGY/G9ROI9iMmkpxrUMc+nmgOyRiHo2gCouUI2djmVyqneIN5aFTUObR40g4buIS5BHeWKgGOYAqUJNwH5iTt7+CShV85JoFytYmQhrA62ZkmbdPE3BCTBMwsKMLdEct0qUQoYdyZPpFoQgBXmwk6Y/4OEwXppxSfVGG7rF69mrFjxzJx4kSuueYahg0bxuLFi3lLa8YoFenyBNFkmKTcayLOUzvqYEO0CvGNhHG1Kjs8aROlTT4emj7iKmEGyMjI4MCBAwAcOHAg4jOoCfVRwlyOK2QZuNmGdqjPtosjN6t2bfKQdme2RMd8qEx5YtuLDa4xM6qQg6jjIYy6LURgy3CSMn4EH67vIuJfwPVfJDsvuarV/VGTJk1i9erVVFRU8Pzzz7Nt2zbCYUNHci2CRCdh2dfH9mNIQtakamx9EXj7+opz0k4AS1ZUmavhoekh7tDi+PHjI6nFy5cv5+qrr67VAU9nCXN3pQhibviQ9QJXoACO48605biNTsCQgMKoyj7n99aYmb55jGOK0CcpOMeRl1Rn2yBGkJoro60I6dhyFdIml8FvzcrirxC1PtnZtgUueVWXDCQJSRdddBGvvfYa7777LjNmzABnWyGboIxf/ATa1URsx2qCZSqBCVHaoUhZN4Dr95BsTtEsQpjz9HDmoEYtbty4caxatYrk5ORIN+Np06axbt06duzYwbx58+jbty9r1qwhHA7z85//nP/85z8899xzZGVl8f777zNixAgGDBgQ2Wc4HD4tJcx+3MSiAI7aD/w/oAdGmOSmFZ+BzNyiETRTENbRam8SRsD9wFK/4o6guavlRk9Xxm4W4ihz9pPmLAPXD1EZf9aaG5SiRLtCajsJbTtfYfZ5jOrDnSkpKaxatYotW7YwY8aMqAQw8fYnKFBO4VZICriUMQ8SnbBqojI+AyEFgaj7IvQBXHMjZB1HCEFqQTx/wZmFGv+vX/3qV2zZsoVu3brx6aef8umnn/Lmm28yf/58jh8/zuuvv05aWhqtWrWqtjNyQoJriWZkZPD3v/+d4cOH12nwkmkYBnbizs6tcPsTgBFwKVOuHHwrde5qH8Z51xxooaCrD/okQKZ1hRIxGkEaJmIhrdPScGfzls66crapwB/DJ0+RYg5IlEPGIBqCHzdk14LqMyIBRo4cycyZM0/KBP2r1vidzEqZsWXWT8UQANZ7sjJkAXCRkzcgEZIK3EQtgWgMdlFYVX4HD00bNZLB0KFDadUqOn9OKcWxY8cAKCoqokOHDkDDhxXFzg0A5+M6tjoSbSY0w7WZxTtfgZnRZSYr00bgkjFEcGkzGNEWspydXKEUIYxP4hiQpUw9xPmOBiFOOskdkIhArBldcg/sqknxe9jOODv60RwYFWfMXv4KO/kpQbm1GQkYQgtoN0ToA9rgZk6KgIuTUBK5ZHmzSt+DuBEUDy7GWgVulykV939aH4jL2bt48WLGjBnDL3/5S8LhMAUFBYAJK95444384Q9/4Nlnn43aZs6cOcyZM4dRo0bVfdQOPtOabCc9GBxVl+gCpWa4gi9OO/EtiM1rN0a1+yNu+w7aWFcojMlyPE9Be5/RII6ETQgwBCRrc5xUjFCUOcevDBEaHDVdSECSeTIVfOeMQcgF6lY3oDGzf7pzHDFPwE2rTrS0AjA+kWLnux0mFV9KGa7gK2e5+E2qS2tuSpBsSVvDCWPOQTJZbSKsfF7raiC8UUpF7js5jhxLJqKxSvF2EyDOuMhg6dKlPPHEE0ycOJFXXnmFadOm8e677zZKZ+TPYlzEHKUi9r/MvgmY2VfehY/lAStBTC9EnPVXlEAbBVsC8L+JbpDSVsHlLaGwDHZUwI6gEaSjmBm2BEMIImSVYdcziGNTSEljOjPZTVnkBo0XcnODaRufolziTLGOnQRUOKGF49a1sMetcKsx0zCEIIIjSV1KwhOngCuV4q1GEAgRdNtPk4hrUorvIxn3/rExQilO4JqsYExFH+4EJCSqnX2UYqJEdf1fTzfiIoPly5dHHrN2/fXXc+utt9ZqOwkr2i3S6gP/inFT/UQpinBNB3BJoATzJ5cCJ7QhhebKNTs2+VRkZpAbpfVN0OobqFgNX4VN38VUzB+fiPnTU4D/E8Nf8GxYc5PPEJbMrkJcImBSTi2zsD27nCoSHZ9BIsYxaucByL5bKqPZAJQodxYstpKQknHNgxaO1pCCESDZb6l2Q7bV4RqlIucj2sREpfhLAxOCkK3ckXZ9iRCkkEUAV6uy09nPxW2hB259jGhN4quR8HeSs9zWTpsC4nL4dujQIdLfcN26dXTv3r1W29VXZ+TaYJ3WUewt8XDpliwpweJI/FYbVT0V408QgW2GmTW3LIXyL+BQ0GxT5IQoS7V7g1UHKYiyKwhTnGXNcTMpj+PG8ONtCfN/w5okK4lKqjrTgA6O76O9ggzl3vgJuEVJokUlKbN+ujJjTFLQ0QlDSoZjijo5NHml0/jlCuuZlzIWuU5+6zgNCSEC8ZPIGCow1weMs7gI819UYHxGpbgkVtk8lWiKndsh/52QTTEmoazY2W9TQI1k0LVrVy644AK2bdtGZmYmv//973nuuef42c9+RnJyMuPGjSMrKyuyfuVmqAcPHoxqhiqdkRujpZc4uoSJpQ7hXMyfJCqcsHYI2K5hn7M8AaPCF2tYXQGPb4cNQfg6bFKixe72YQikpm6PZdqN8YtmEMRt3CpjPeqsHwKGxulwkpi/hGBlxmutoHeCOVayMuOvXJHpww2nFmFU3AxlfCdHMX4PH27UI6QNOdrdn2RWtMkYXOEL0vC9FC5xCsdkHJXJ9gTmfw/j1mXIf1qKIewSZz0hb9EKxKQQn4OEwW3Hq0S/6qcr6KmjRjJ44YUX2Lx5M1lZWezdu5dp06YRCATo2bMnx44do7S0lMceewyI3Qw1Ly8vqhmqdEbu1atXPZ5WbGzSOpJCbDu5juLeDKXOSxhbbEfJUQDD6P/R8KWG/2qjRQQcTUJm9wTl3iCxIMk9cjOIWVCGO3vIjNEOd71S4kMIh3y0WxmZrow5dHEq/PwiuDTRtXXLtUsakg9xrhM9yU4wz6f8EUZTEA1H/DPpynWKRqIXRFdr2qqxkI5glFLkOK/6hF3NKWFTMY9EaGW2F1K08yxEYxASK8LcN9Kh29Z+RPDlPpNrIfdUfZ9rbRBXaHHp0qXMmjWL5GRjgUsqcmM0Qz1V2F2RJXZuZy5KApBEA4owamEFRjiPYfwDFdoI17eOYBXjNk+JNDapZqp7PqyjPMzijbcjIco5fnvl3kDxelvslm9yvt9row20+1/w7sfQLsmsV465cYu160U/B8jxwR0t4IpW0C3BFRpRp6XeQ2or5HcxO+xy6ViJTbbwyON4spXiIqsceqhS/Pg0CY4kpEG0o1YS2aQpjuR/2NqNbFcKfAN8hbk/0nArWCs3vrH/V5sQhXAbG3H5DHbs2MGGDRsYNGgQw4YNY/PmzUDjNEM9VcgNIEIvlY6SmCQqeyFGBZTKRzEVKjDhxSLnXXoPyB8cxpCE1vCnGpxhQStsJ0KTguvkLMXcJJ9qK306zvMWTchOPS4BdoZgz+9gRA9484TRbkS1FbNFSr3P80PG3fCj1/z0SDZaRUAbZ6I4y6RaU5Km7GQkO3Rnpy6nWp+vsQS9NSav4xyMh74lrtD82CEFIYphdSAImaXBnf1l1g7jmFC4jXHk/7JrYLpgJoQTzvIkZzu/811GJ2Qs5Ci9N5pCVCEuQgoGgxQWFrJp0yY2b97MDTfcwM6dO1FKNXgz1FOFtl72HwTubCZCI3n7wphCGHKzSEFOyPosvoLYbUaiIRqECHoAQzKy73KcLEHcB8PEiz9rzbVOzFtMBp+CHRqWngC+MEQQwERUxHMuN2sAOByC8DbwNQuyp8L4BaR0/DhupEVmf2k1J85Xud62miznKq+w9bIzMsVMS8OQgr39IWe/IxxC+N4Z98ZaRCZiRWnsvpnNiCZgIQRJKpPfCzHXohOuiQFuerrdVavC2pdoIo2fZRAnGWRmZjJhwgSUUgwcOBCfz8eRI0do06ZNtdvVRzPUU8U/tGagU3gjXmC7958tcDIL2N2QJdykrHXtzkmiPdRmBi/GDTPJ8cDM2CmYm14EUfwcYBJlNsURgivBbUZSgdFMlLM8EWMWiD0sWok4ylKBz0Jw8G/QegXsc5Kt0jDCJ01mFbDcCade6UQN7KSpyuaBhOxsn0IkbRrX1BD/hczUYCIXEoWRVwLm4Tn+GhSFyo5Ye3IA1/yRjttC+jbJyxibY0wFnPVaK3dSsE0BOU87gUsIr3H1ZYO4zIRrrrmGdevWAcZkqKio4Nxzz61xu/pohhoPJAxm24EQPSuJliAhRzt1WWZ++7N46WXGEMdUdZDMSCEP8U7bIUc7dm3PLvEgCaPGluOGVcsxodJ92vV7SPemcmf9Cm2crAc0bA/DjhD8N2x8DoeddWPNsHaEQK6lnIMIiS0EIU6Owcs1ln0kYK6T3zJNWmAEMl2ZfIe2KnY9iA37etp1HzKrS8hQZv7KBAZumXlz4ELnlYKJukB0erqMVQhPclzKcCIymCrcxkRcocVbbrmFnTt30q5dO3r27MmTTz6JUqrBm6HGi1LcWUZuOmF8+R62fpebVGY+sfuw1pNbT8JMq2oxc/9d6ygHZiGuWim+C5lN5LOYOPFAKjKTcENiJzCmSal1/BDRlZL7Mc7QUuAgJoLynbOPEsuBKrOcwPajiE/EdtTa/gQ73CtJV0IwSbg5DyKssq8fYdKmO/mgq5MvUdsZztZCRGhDGEIJOddGQoB2opFoCuLnkFm/vbPsOOYBwHY3LHkOqPgh7GzGdGdZ7XuM1w/iCi0mJSWxcOFCcnJyOP/88yOpxo3RDDUeSPqtnRwkAi5agW3H2najFBRhbSemhMy2tfEX2GMRIZNy42a4jU4g2mdhRz5OFWuc0KocV2ZdW2WVBCxxgKVhZt4i4LATRdnv5FoUaTenAgwxlFlMJbkbdkhOhKkyRLjDQAdcbczWvkRbEt9NC0y+QycFw5JcDa11LSZYO6NQZm0w17xcu5EEmdVtQpDttbPdcYypJP//Tm3GdI4ypCAQE8GOGMn/2RTKveMKLQLce++9PPLII1Elsw1dtRgvJGxo5xCEcRNMRC0N4T5gJAE3GcjWGGR9+SxEU1uIYNsFTRKntscnsBuzxANpuGpHVeSfEkETk0i0hCPOduW4kRNZp1TetWvqYO3PdhBK6reQrcTiZYa1zzPR2lZX+q0ZJsJwjjKh0fwcyJ0Bg5KM2l4bMrCdmPLfSVKRhPpEOxFiEoKQ0nKpVrWrYX0YAm2rDFlJSz6pU7HTj20naVNAXObnihUr6NixIzk5OVHLG7pqMV5sdJyI8udJCFEgZgHOb3ssYuvtOMXscJDM6HKDn8pFLcZVJ2V2FKGsHPEIQ5V9EGuLo7XcT55SkUfEJeFWYAoZydhk5pbf7GKjNVpzmTNZBDAq/VFrnyHM9ZWwHZycy1/Z7LAdbgEnMUolAa2hlR+Oldec3ivt24W87UzBDspN1RaCEy1JnKrixPweU+bdHENOKbhJR2CyOQPanSDsh/zIucoYTkWbrC+cMhmUlJSwYMEC3nnnnZN31ghVi/HCvmFEmOVG212NoHxZzW/9nNDdqaSXHsN9DJvPWiY3fl2FvzK21HJ/laMVP1YqkitQeSZLxq3riAXRpL611hUhE+1EYIcXhQAl4uG3tinF/b+e2wxtt7jCu6C6bC9rTHI8+a4xvhFRdkVIxdwR0pBoQCom/6QTxiw4od2mOokOUxZrtxjN7rIlfgr7nH2YJKtYlbgNgVMmg6+//ppdu3ZFtIK9e/fSv39/PvzwQ9q1a1fttg1VtVgb2Grh16fp4scjuP/Wmn5KRUply4ldlt3YqI5ELlEqog5XhtzwdpqvaAGiYYiGAC5J2NEJsdcTccOjJzQElWvfF4bMb3trcelsQZR3IbI0ZapWTzjf/RgBt3cr2kEKhsiLnXGlKbfK8y9B6KLcKJNkmIr/Q0wPv7VPcVw3Fk5ZKvv27Rt5RgJA586d+eijj2oVWhw9ejRz585l//79p3rY047tTUjgTvfs39D4Zw3jt8NqpUTnE0D07GwX7lTOTxASOYFjlmiz/kFnXycwTs6aUJkA7DqK4zo6VV3qE0qt45c7n8U8SsY4T1FG4I85y49iSEIci5KZKX4DaeQrE1MLGtd/EFdoccaMGfTq1Yvs7GwOHz5MUVFRZP2mXLXooeGxVuvIrCozPriOSjEZbD9EpLbDWVc6O9tpvOW45cQHtSGBvbp2YVchJlvzEA1FHoIjxy52lklbtwBuenGqs550ugppYzbI/kq0W/ko2xQ5Y2+Gm1UpPSGgbs7huiKu0OKoUaPYunUrn332GXfddVfEWdjUqxY9NA4kWmCHFu0wm6Tj2g+PkUfe2V5/eeqU/S72vHxeU4OWcpHVS0FufiGfdkT7EiTMK2QkM7id6OTH9IkswzgUJQlJIi7iZ5FnZ4o/6ChupMJugVZdS/z6RlyhxdGjR0fs/ry8vEjb8zOhatFDw0NmYZltJV8g0mqN6KQf2aYINwNUHI0luGRRYv2+i9g+i8oQIpJkKNEINC6ptFQmm9EmKjFhlDVmCb/apk0LnMpW53tL3GdwSKs4cUoWAxN8bjYszrlc1kiZiHU2UZYtW8bll18OnBlVix4aHseIrukXAda4wi0zvPwuNja4OReSByI2u/gcghihq00UR6oq7bRoOx09hEmsKtGuNmL3wLD1jhBmVl+jNeu1Jhk3ylEKvK11RKuw2+2B0TgSgVfDRqOQorQ0azwNjTq59SUyMHXq1Miypl616KHhkYhR6+3sO1sLkO8ioIXOewiT49FTqYgtLfsQlV0iA7ZQVwc79dhOdJL92TF/SaCyw5/2cSpnDq6IYaKINiF+BYDeyjTGsVuiJWJIoRWNF1GImwyWL1/OypUree+99056cEdVaApVix4aHh84SV529qSo3iLQYpPbtQuCWJGfHKUiT3mS9RNOWutkCBFUPob92a4HsZ2aUs4ty+yK0+qOpzEz/gnn837ndFIxxHcOrhkiJeyNgbjI4O233+bhhx9m/fr1pKbWvrzCrlocOHBgPIf2cIbCFio7HTeMOwPbxWI1qfyxOmDXFnZXIzCE1EPBUe0mFIkfQDIPZbk4+FKccVZW/2MdS/ISkjDmgN1ePhk3kiIa0geNFGqOK7R45513snPnTjIzM0lPT+fmm28GOGOqFj00PIQIJD9faiPs1NxvgP/gxvcrPwX7dEDa3cuMLfUSn2qTr2D3PWxBdN9G+c1uaFPTU6Zts0iSj2wtxI9Jj9+kNR84742FGjWDF154gebNm3PTTTexdetWALZv306rVq2YNWsWv/3tbyksNFaeVC0++uij3H///SxbtiyqavGqq65qElWLHhoetjoMbg2CePPBPCJPuha1JFpoThf+Xo2wDVaKr3CfD5GASTP+o9Ws5VSfc/BXrRnjpHKDcaY21SSzGslg6NCh7N69O2rZG2+8EXkic35+PsOHD+fhhx+utmoxEAiwdu3aJlGo5KFx4Mc4ycQJKOZBIsbBJk0/pGQ8HTal+uEAAAfQSURBVFMTUdt6irqipjZp8T7xqabch6aCuEKLhw4don379gC0b98+kp48ZswY1q9fz/jx45k+fXrUNnPmzGH+/Pl1HK6HMxX/1pqjRHeAksSjUkzHJDEPmuN2o2ooIvBwmjs0n0lVix4aHi0wmXdJwHagK27peHNM96RyT/gbDXFpBhkZGRw4cACAAwcORJ6bUBOkatHD2YljuE9qbo/RCP6tNf/Wmu+09oigkREXGYwfPz6SWrx8+XKuvvrqWm3XmM9a9ND42K41/3KEf7fz8tB0UCMZTJkyhcGDB7N9+/ZIaHHWrFmsXbuW7t27s3btWmbNmlXrA0rVogcPHpoWlNZNk54rFzt58OAhGqdbRhq/5VA1qG2aswcPZyM6dep0WvfXZMnA0wo8eGhYNJUuzR48eGhkeGTgwYMHwCMDDx48OPhBkcEtt9xC27Zt6dOnT2TZpEmTyM3NJTc3l86dO5Obmxv5beHChXTr1o2ePXuyZs2ayPKXX36Z/v37s3jxYgCWLFnCPffcE/n99ttv57LLLot8f+qpp7j77rurHYes17NnT7Kyspg5c2ajjGPevHl07Ngxck1WrVpVr+Oo6loAPPbYYyilOHLkCGD8RHfffTfdunUjOzubjz/+OLLuE088Qf/+/fnzn/8MmCd6yXjApMLfeuutke/33Xcfjz/+eLXXYu7cuWRnZ5Obm8vo0aMjXbsbehx2g+Frr72W77//PvJbfd0bMaF/QFi/fr3esmWLzsrKivn79OnT9YMPPqi11nrbtm06Oztbl5WV6Z07d+quXbvqYDCotdb66quv1sFgUE+aNEkfP35cf/jhh/qiiy6K7GfgwIF6wIABkfUnT56sX3755WrHsW7dOj1y5EhdVlamtdb60KFDjTKOBx54QD/66KMnXZv6GkdV/8k333yjR48erc8//3z97bffaq21fuutt/TYsWN1OBzWGzdu1AMHDtRaa338+HE9ZcoUHQgE9Pjx47XWWr/yyiv6+uuv11prHQqFdP/+/XVeXl5k/3l5eXrTpk3VXouioqLI5yVLlujbb7+9UcaxZs0aHQgEtNZaz5w5U8+cObNe/5Oq8IPSDKp6LiQYtn/llVeYMmUKYCovJ0+eTHJyMl26dKFbt258+OGHkXXBjeP269ePHTt2UFpaSlFREampqeTm5vL5558DUFBQwJAhQ6odx9KlS5k1axbJyaYdhqRwN/Q4qkJ9jeNUntX5xhtvcNNNN6GUIi8vj++//54DBw5EHV9w8cUXU1BQAMC2bdvo06cP6enpFBYWUl5ezpdffkm/fv2qvRYtWrgdE06cOBHZf0OPo6oGw/V5b8TCD4oMqsOGDRvIyMige/fuAOzbt4/zzjsv8ntmZib79u0DYMKECQwYMIABAwaQnp6O3+8nNzeXzZs3s2nTJgYNGkReXh4FBQXs378frXXUvmJhx44dbNiwgUGDBjFs2DA2b97cKOMAePrpp8nOzuaWW26J9KJoyHFU9azOqsaQnp5O3759GTBgAJMmTQKgQ4cO+P1+vvnmGwoKChg8eDCDBg1i48aNfPTRR2RnZ5OUlERNmD17Nueddx4vvfQSv/71rxttHAK7wXBD3xtNNs/gdONPf/pTRCuA2HkMwvj5+fnk5+dH/SYzQGlpKYMHD6Z79+785je/oU2bNjUyLkAwGKSwsJBNmzaxefNmbrjhBnbu3Nng47jjjjuYO3cuSinmzp3Lfffdx7JlyxpsHNU9q7O6Mdx///3cf//9McdQUFDA9OnT2bdvHwUFBbRs2bJW1wJMU98FCxawcOFCnn76aR588MFGGYeMxW4w3ND3xlmhGQSDQV5//fUIm4Nh2T179kS+7927lw4dOlS5jyFDhlBQUMDGjRsZPHgwvXv35osvvqCgoICLL764xjFkZmYyYcIElFIMHDgQn8/HkSNHGnwcGRkZJCQk4PP5uO222yJqZ0ONw35WZ+fOnSPP6jx48GDcY/j888/p06cPeXl5bNy4sdbXwsZPf/pT/vKXvwDxX4u6jEMaDL/00ksRgW/oe+MH5UDUWutdu3ad5KxavXq1Hjp0aNSyrVu3RjlnunTpEnG2xMLRo0d1mzZtdG5ubmTZ6NGjdZcuXfRHH31U4ziWLl2q586dq7XWevv27TozM1OHw+EGH8f+/fsjnx9//HE9adKker8esf4TQadOnSIOxJUrV0Y57myHWCx88sknukuXLnrkyJGRZf3799cZGRmRfVY3jh07dkQ+P/nkk3rixImNMo7Vq1fr3r1768OHD0etV9/3RmX8oMhg8uTJul27dtrv9+uOHTvq559/XmutdX5+vl66dOlJ68+fP1937dpV9+jRQ69atarG/V944YV66tSpke8PPPCATktLi3iCqxtHeXm5njp1qs7KytL9+vXT7733XqOM48Ybb9R9+vTRffv21VdddVUUOdTHOKr6TwQ2GYTDYX3nnXfqrl276j59+ujNmzdXe/xgMKjT09P17NmzI8vy8/N1jx49Tlo31jgmTJigs7KydN++ffW4ceP03r17G2UcF1xwgc7MzNQ5OTk6JycnEtXQuv7ujVhoslWLHjx4aFicFT4DDx481AyPDDx48AB4ZODBgwcHHhl48OAB8MjAgwcPDjwy8ODBA+CRgQcPHhx4ZODBgwcA/j+FV5ohepXOTwAAAABJRU5ErkJggg==' style='max-width:100%; margin: auto; display: block; '/>




```python

```
